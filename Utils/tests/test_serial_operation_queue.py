from threading import Thread
from time import sleep
from unittest import TestCase
from unittest.mock import Mock, MagicMock
from Utils.serial_operation_queue import SerialOperationQueue


class SerialOperationQueueTestCase(TestCase):

    def tearDown(self):
        if self.__test_thread is not None and self.__test_thread.is_alive():
            self.__test_thread.join()
            self.__test_thread = None

    def __perform_method_on_background_thread(self, selector):
        if callable(selector):
            self.__test_thread = Thread(target=selector)
            self.__test_thread.start()

    def test_running(self):
        queue = SerialOperationQueue()
        self.__perform_method_on_background_thread(queue.start)
        sleep(1)
        self.assertTrue(queue.running)

        queue.stop()
        sleep(1)
        self.assertFalse(queue.running)

    def test_add_execute(self):
        mock_object = Mock()
        mock_object.some_method = MagicMock()

        queue = SerialOperationQueue()
        self.__perform_method_on_background_thread(queue.start)
        queue.add_executable(mock_object.some_method, False)
        sleep(1)

        mock_object.some_method.assert_called_once_with()

        queue.stop()
        sleep(1)

    def test_run(self):
        queue = SerialOperationQueue()

        self.__perform_method_on_background_thread(queue.start)
        sleep(1)
        self.assertTrue(queue.running)

        try:
            queue.start()
        except Exception:
            pass
        self.assertRaises(Exception)

        queue.stop()

    def test_stop(self):
        queue = SerialOperationQueue()
        self.__perform_method_on_background_thread(queue.start)
        sleep(1)

        queue.stop()
        sleep(1)
        self.assertFalse(queue.running)

    def test_flow_repeatable_executable(self):
        mock_object = Mock()
        mock_object.some_method = MagicMock()

        queue = SerialOperationQueue()
        self.__perform_method_on_background_thread(queue.start)
        queue.add_executable(mock_object.some_method, True)
        sleep(1)

        queue.stop()
        sleep(1)

        self.assertTrue(mock_object.some_method.call_count > 1)
