import argparse

import sys

import os
from threading import Thread

from Connection.connection_manager import ConnectionManagerConfiguration, ConnectionManager, ConnectionManagerDelegate

SERVER_CONFIG_FILE_PATH = ''
CONNECTIONS_COUNT = 0


def exit_with_error_message(error_message):
    if error_message is None:
        error_message = 'unknown error.'
    sys.exit('Error: ' + error_message)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Server arguments')
    parser.add_argument('--configuration', dest='config_file_path')
    args = parser.parse_args()

    config_file_path = args.config_file_path
    if config_file_path is None or not os.path.isfile(config_file_path):
        exit_with_error_message('invalid config file path')

    global SERVER_CONFIG_FILE_PATH
    SERVER_CONFIG_FILE_PATH = config_file_path


class MockConnectionManagerDelegate(ConnectionManagerDelegate):
    def did_receive_connection(self, connection_identifier: int):
        print('Did receive connection with id: ' + str(connection_identifier), flush=True)

    def did_close_connection(self, connection_identifier: int):
        print('Did close connection with id: ' + str(connection_identifier), flush=True)
        pass

    def did_receive_data(self, received_data: bytes, connection_identifier: int):
        print('Did receive message: ' + received_data.decode() + '. From connection wit id: '
              + str(connection_identifier), flush=True)


class Command:
    __name = ''
    __description = ''
    __executable = None

    def __init__(self, name, description, executable):
        self.__name = name
        self.__description = description
        self.__executable = executable

    def __new__(cls, name, description, executable):
        result = None

        if name is not None and len(name) > 0 \
                and description is not None and len(description) > 0 \
                and executable is not None and callable(executable):
            return super().__new__(cls)

        return result

    def __str__(self):
        return self.__name + ': ' + self.__description

    def execute(self):
        self.__executable()


# DEFINE GLOBAL VARIABLES HERE!
CONNECTION_MANAGER = None
CONNECTION_MANAGER_THREAD = None
CONNECTION_MANAGER_DELEGATE = None


# DEFINE COMMANDS EXECUTABLE HERE!


def start_connection_manager():
    global CONNECTION_MANAGER
    if CONNECTION_MANAGER is not None and CONNECTION_MANAGER.running:
        print('Connection manager is already running!')
    else:
        print('Starting connection manager...', flush=True)

        configuration = ConnectionManagerConfiguration.configuration_with_file_name(SERVER_CONFIG_FILE_PATH)

        if not configuration.is_valid:
            exit_with_error_message('configuration is not valid!')

        connection_manager_delegate = MockConnectionManagerDelegate()

        connection_manager = ConnectionManager()
        connection_manager.configuration = configuration
        connection_manager.delegate = connection_manager_delegate

        CONNECTION_MANAGER = connection_manager

        global CONNECTION_MANAGER_THREAD
        global CONNECTION_MANAGER_DELEGATE

        connection_manager_thread = Thread(target=connection_manager.start)
        CONNECTION_MANAGER_THREAD = connection_manager_thread
        connection_manager_thread.start()

        CONNECTION_MANAGER_DELEGATE = connection_manager_delegate

        print('started', flush=True)


def stop_connection_manager():
    global CONNECTION_MANAGER
    if CONNECTION_MANAGER is None or not CONNECTION_MANAGER.running:
        print('Connection manager is not running!')
    else:
        print('Stopping connection manager...', flush=True)
        CONNECTION_MANAGER.stop()

        global CONNECTION_MANAGER_THREAD
        global CONNECTION_MANAGER_DELEGATE

        CONNECTION_MANAGER_THREAD.join()
        CONNECTION_MANAGER_THREAD = None
        CONNECTION_MANAGER_DELEGATE = None
        CONNECTION_MANAGER = None

        print('stopped', flush=True)


def exit_from_program():
    stop_connection_manager()
    sys.exit()


def print_help():
    global COMMANDS
    for command_name in COMMANDS:
        print(str(COMMANDS[command_name]))


COMMANDS = None


def parse_input(user_input):
    if user_input is not None:
        command = COMMANDS.get(user_input)
        if command is None:
            print('Unknown command: ' + user_input + '!')
        else:
            command.execute()


def init_commands():
    global COMMANDS

    COMMANDS = {
        'help': Command('help', 'print info about all commands', print_help),
        'start': Command('start', 'start connection manager with Mock delegates', start_connection_manager),
        'stop': Command('stop', 'stop connection manager with Mock delegates', stop_connection_manager),
        'exit': Command('exit', 'exit from program', exit_from_program)
    }


def main():
    # a = b'asdasdc\r\n\r\nasdfasdfa'
    # b = b'asdfasdf\r\n'
    # c = b'asdfkjashdkfjas'
    #
    # print(a.split(b'\r\n'))
    # print(b.split(b'\r\n'))
    # print(c.split(b'\r\n'))
    #
    # for i in range(0, 5):
    #     print(i)

    parse_arguments()
    init_commands()

    running = True
    while running:
        user_input = input()
        parse_input(user_input)
        print('', flush=True)


if __name__ == '__main__':
    main()
