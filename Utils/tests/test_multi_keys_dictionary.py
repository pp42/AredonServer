from unittest import TestCase
from unittest.mock import Mock

from nose.tools import assert_raises

from Utils.multi_keys_dictionary import MultiKeysDictionary


class MultiKeyDictTestCase(TestCase):
    def test_new(self):
        multi_key_dict = MultiKeysDictionary(-1)
        self.assertIsNone(multi_key_dict)

        multi_key_dict = MultiKeysDictionary(None)
        self.assertIsNone(multi_key_dict)

        multi_key_dict = MultiKeysDictionary(0)
        self.assertIsNone(multi_key_dict)

        multi_key_dict = MultiKeysDictionary(2)
        self.assertIsNotNone(multi_key_dict)

    def test_set(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value)
        self.assertEqual(multi_key_dict.count, 1)

        self.assertEqual(value, multi_key_dict.get_by_key_at_index(key1, 0))
        self.assertEqual(value, multi_key_dict.get_by_key_at_index(key2, 1))

        multi_key_dict.set([key1, None], None)
        self.assertIsNone(multi_key_dict.get_by_key_at_index(key2, 1))

        with assert_raises(Exception):
            multi_key_dict.set([key1, Mock()], Mock())

        with assert_raises(Exception):
            multi_key_dict.set([Mock(), Mock(), Mock()], Mock())

    def test_set_by_key_at_index(self):
        multi_key_dict = MultiKeysDictionary(2)

        value1 = Mock()
        value2 = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value1)
        multi_key_dict.set_by_key_at_index(key1, 0, value2)

        self.assertEqual(multi_key_dict.get_by_key_at_index(key2, 1), value2)

        with assert_raises(Exception):
            multi_key_dict.set_by_key_at_index(Mock(), 1, Mock())

        with assert_raises(Exception):
            multi_key_dict.set_by_key_at_index(Mock(), 2, Mock())

    def test_get(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value)

        self.assertEqual(value, multi_key_dict.get([key1, None]))
        self.assertEqual(value, multi_key_dict.get([None, key2]))

        with assert_raises(Exception):
            multi_key_dict.get([Mock(), Mock(), Mock()])

        with assert_raises(Exception):
            multi_key_dict.get([Mock()])

        with assert_raises(Exception):
            multi_key_dict.get([])

    def test_get_by_key_at_index(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value)

        self.assertEqual(value, multi_key_dict.get_by_key_at_index(key1, 0))
        self.assertEqual(value, multi_key_dict.get_by_key_at_index(key2, 1))

        with assert_raises(Exception):
            multi_key_dict.get_by_key_at_index(None, 0)

        with assert_raises(Exception):
            multi_key_dict.get_by_key_at_index(key1, 2)

        with assert_raises(Exception):
            multi_key_dict.get_by_key_at_index(key1, -1)

    def test_remove(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value)
        multi_key_dict.remove([key1, None])
        self.assertEqual(multi_key_dict.count, 0)

        multi_key_dict.set([key1, key2], value)
        multi_key_dict.remove([None, key2])
        self.assertEqual(multi_key_dict.count, 0)

        with assert_raises(Exception):
            multi_key_dict.remove([Mock(), Mock(), Mock()])

        with assert_raises(Exception):
            multi_key_dict.remove([Mock()])

        with assert_raises(Exception):
            multi_key_dict.remove([])

    def test_remove_by_key_at_index(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        multi_key_dict.set([key1, key2], value)
        multi_key_dict.remove_by_key_at_index(key1, 0)
        self.assertEqual(multi_key_dict.count, 0)

        multi_key_dict.set([key1, key2], value)
        multi_key_dict.remove_by_key_at_index(key2, 1)
        self.assertEqual(multi_key_dict.count, 0)

        with assert_raises(Exception):
            multi_key_dict.remove_by_key_at_index(None, 0)

        with assert_raises(Exception):
            multi_key_dict.remove_by_key_at_index(key1, 2)

        with assert_raises(Exception):
            multi_key_dict.remove_by_key_at_index(key1, -1)

    def test_count(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        self.assertEqual(multi_key_dict.count, 0)

        multi_key_dict.set([key1, key2], value)
        self.assertEqual(multi_key_dict.count, 1)

        multi_key_dict.remove([key1, None])

        self.assertEqual(multi_key_dict.count, 0)

    def test_contains(self):
        multi_key_dict = MultiKeysDictionary(2)

        value = Mock()
        key1 = Mock()
        key2 = Mock()

        self.assertFalse(multi_key_dict.contains(value))

        multi_key_dict.set([key1, key2], value)
        self.assertTrue(multi_key_dict.contains(value))

    def test_iteration(self):
        multi_key_dict = MultiKeysDictionary(2)
        values = list()
        values_count = 5

        for i in range(values_count):
            value = Mock()
            key1 = Mock()
            key2 = Mock()

            multi_key_dict.set([key1, key2], value)
            values.append(value)

        counter = 0
        for value in multi_key_dict:
            counter += 1
            self.assertTrue(value in values)

        self.assertEqual(counter, values_count)

    def test_getitem(self):
        multi_key_dict = MultiKeysDictionary(2)

        first_key = 2
        second_key = 'abc'

        multi_key_dict[first_key, second_key] = 4
        a = multi_key_dict[None, second_key]
        b = multi_key_dict[first_key, None]
        c = multi_key_dict[None, None]

        self.assertIsNone(c)
        self.assertEqual(a, 4)
        self.assertEqual(b, 4)

    def test_setitem(self):
        multi_key_dict = MultiKeysDictionary(2)

        key1 = Mock()
        key2 = Mock()

        multi_key_dict[key1, key2] = 4

        self.assertEqual(multi_key_dict[key1, None], 4)

        multi_key_dict[None, key2] = 5
        self.assertEqual(multi_key_dict[key1, None], 5)

        with assert_raises(Exception):
            multi_key_dict[key2, None] = Mock()
