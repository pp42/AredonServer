import string


class Logger:
    @staticmethod
    def log(msg: string):
        if msg is not None:
            print(msg, flush=True)
