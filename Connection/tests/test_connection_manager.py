from threading import Thread
from time import sleep
from unittest import TestCase
from unittest.mock import Mock

from Connection import ConnectionManager
from Connection.connection_manager import ConnectionManagerConfiguration, ConnectionManagerDelegate

TEST_CONNECTION_MANAGER_LISTENING_PORT = 10678
TEST_CONNECTION_MANAGER_LOCALHOST = '127.0.0.1'
TEST_CONNECTION_MANAGER_LISTENING_COUNT = 20
TEST_CONNECTION_MANAGER_MESSAGE_SEPARATOR = '\r\n'


class ConnectionManagerTestCase(TestCase):
    __test_thread = None

    class TestConnectionManagerDelegate(ConnectionManagerDelegate):
        received_connection_id = None
        closed_connection_id = None
        received_message = None

        def did_receive_message(self, message, connection_identifier):
            self.received_message = message
            self.received_connection_id = connection_identifier

        def did_close_connection(self, connection_identifier: int):
            self.closed_connection_id = connection_identifier

        def did_receive_connection(self, connection_identifier: int):
            self.received_connection_id = connection_identifier

    def tearDown(self):
        if self.__test_thread is not None and self.__test_thread.is_alive():
            self.__test_thread.join()
            self.__test_thread = None

    def __perform_method_on_background_thread(self, selector):
        if callable(selector):
            self.__test_thread = Thread(target=selector)
            self.__test_thread.start()

    ####################################################################################################################
    # TESTS

    def test_init(self):
        connection_manager = ConnectionManager()
        self.assertIsNotNone(connection_manager)

    def test_set_configuration(self):
        connection_manager = ConnectionManager()
        configuration_mock = Mock(ConnectionManagerConfiguration)
        connection_manager.configuration = configuration_mock
        self.assertIsNotNone(connection_manager.configuration)
        self.assertEqual(connection_manager.configuration, configuration_mock)
        connection_manager.configuration = None
        self.assertIsNone(connection_manager.configuration)

    def test_set_delegate(self):
        connection_manager = ConnectionManager()
        connection_manger_delegate_mock = Mock()
        connection_manager.delegate = connection_manger_delegate_mock
        self.assertEqual(connection_manger_delegate_mock, connection_manager.delegate)

        connection_manager.delegate = None
        self.assertIsNone(connection_manager.delegate)

    def test_running_with_valid_configuration(self):
        configuration_mock = Mock(ConnectionManagerConfiguration)
        configuration_mock.is_valid = True
        configuration_mock.localhost = TEST_CONNECTION_MANAGER_LOCALHOST
        configuration_mock.port = TEST_CONNECTION_MANAGER_LISTENING_PORT
        configuration_mock.listening_count = TEST_CONNECTION_MANAGER_LISTENING_COUNT
        configuration_mock.message_separator = TEST_CONNECTION_MANAGER_MESSAGE_SEPARATOR

        connection_manager = ConnectionManager()
        connection_manager.configuration = configuration_mock

        self.assertFalse(connection_manager.running)
        self.__perform_method_on_background_thread(connection_manager.start)
        sleep(1)
        self.assertTrue(connection_manager.running)
        connection_manager.stop()
        sleep(1)
        self.assertFalse(connection_manager.running)


class ConnectionManagerConfigurationTestCase(TestCase):
    __TEST_CONFIGURATION_FILE_NAME = 'test_server.conf'

    def test_load_configuration(self):
        configuration = ConnectionManagerConfiguration.configuration_with_file_name(self.__TEST_CONFIGURATION_FILE_NAME)
        self.assertIsNotNone(configuration.localhost)
        self.assertIsNotNone(configuration.port)
        self.assertIsNotNone(configuration.listening_count)

        configuration = ConnectionManagerConfiguration.configuration_with_file_name(None)
        self.assertIsNone(configuration)

        configuration = ConnectionManagerConfiguration.configuration_with_file_name('')
        self.assertIsNone(configuration)

    def test_is_valid(self):
        configuration = ConnectionManagerConfiguration.configuration_with_file_name(self.__TEST_CONFIGURATION_FILE_NAME)
        self.assertTrue(configuration.is_valid)
