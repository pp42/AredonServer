from threading import RLock, Event


class SerialOperationQueue:
    class __Operation:
        def __init__(self, executable, repeat, **kwargs):
            self.executable = executable
            self.repeat = repeat
            self.kwargs = kwargs

        def __new__(cls, executable, repeat, **kwargs):
            if executable is not None and callable(executable):
                return super().__new__(cls)

    def __init__(self):
        self.__running = False
        self.__queue = list()
        self.__sync_lock = RLock()
        self.__should_running_event = Event()

    def __set_should_running_event_if_needed(self):
        if not self.__should_running_event.is_set():
            self.__should_running_event.set()

    # PUBLIC

    def start(self):
        if self.__running:
            raise Exception('operation queue is already running!')

        self.__running = True
        while self.__running:
            self.__should_running_event.wait()  # passive waiting until not empty
            self.__sync_lock.acquire()

            operations = self.__queue.copy()
            self.__queue = [operation for operation in self.__queue if operation.repeat]

            self.__sync_lock.release()

            if len(operations) == 0:
                self.__should_running_event.clear()

            for operation in operations:
                operation.executable(**operation.kwargs)

        for operation in self.__queue.copy():
            operation.executable(**operation.kwargs)

    def stop(self):
        self.__running = False
        self.__set_should_running_event_if_needed()

    def add_executable(self, executable, repeat, **kwargs):
        operation = SerialOperationQueue.__Operation(executable, repeat, **kwargs)

        if operation is not None:
            self.__sync_lock.acquire()
            self.__queue.append(operation)
            self.__set_should_running_event_if_needed()
            self.__sync_lock.release()

    @property
    def running(self):
        return self.__running
