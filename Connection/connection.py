import socket
from threading import RLock


class Connection:
    __DEFAULT_RECEIVE_BUFFER_SIZE = 1024

    __EOF = ''
    __ENCODED_EOF = __EOF.encode()

    def __init__(self, sock: socket, identifier, message_separator):
        super().__init__()

        self.__message_separator = message_separator
        self.__identifier = identifier
        self.__input_buffer = None
        self.__output_data_lock = RLock()
        self.__output_data = None
        self.__socket = sock

    def __new__(cls, sock, identifier, message_separator):
        result = None

        if identifier is not None and sock is not None and message_separator is not None and len(message_separator) > 0:
            result = super().__new__(cls)

        return result

    def close(self):
        if self.__socket.fileno() != -1:
            self.__socket.close()

    def fileno(self):
        return self.__socket.fileno()

    @property
    def identifier(self):
        return self.__identifier

    def receive(self, buffer_size: int=__DEFAULT_RECEIVE_BUFFER_SIZE):
        received_messages = list()
        connection_closed = False

        input_data = self.__socket.recv(buffer_size)
        if input_data is not None and input_data != self.__ENCODED_EOF:
            encoded_separator = self.__message_separator
            separated_input_data = input_data.split(encoded_separator)

            if self.__input_buffer is not None:
                separated_input_data[0] = self.__input_buffer + separated_input_data[0]
                self.__input_buffer = None

            pieces_count = len(separated_input_data)
            last_piece = separated_input_data[pieces_count - 1]

            if last_piece == b'':
                self.__input_buffer = bytearray()
            else:
                self.__input_buffer = bytearray(last_piece)

            pieces_count -= 1

            for i in range(pieces_count):
                piece = separated_input_data[i]
                if piece != b'':
                    received_messages.append(piece)
        else:
            self.close()
            connection_closed = True

        return received_messages, connection_closed

    def send_message(self, message: bytes):
        if message is not None and len(message) > 0:
            self.__output_data_lock.acquire()
            if self.__output_data is None:
                self.__output_data = message + self.__message_separator
            else:
                self.__output_data += message + self.__message_separator
            self.__output_data_lock.release()

    def flush(self):
        self.__output_data_lock.acquire()
        output_data = self.__output_data
        self.__output_data = None
        self.__output_data_lock.release()

        if output_data is not None and len(output_data) > 0:
            self.__socket.send(output_data)
