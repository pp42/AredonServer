class MultiKeysDictionary:
    class __Node:
        def __init__(self, keys, value):
            self.keys = keys.copy()
            self.value = value

        def __new__(cls, keys, value):
            if keys is None or len(keys) == 0 or None in keys:
                return None

            return super().__new__(cls)

    class __Iterator:
        def __init__(self, nodes):
            self.__nodes = nodes.copy()
            self.__index = 0

        def __new__(cls, nodes):
            return super().__new__(cls)

        def __iter__(self):
            return self

        def __next__(self):
            try:
                result = self.__nodes[self.__index].value
                self.__index += 1
            except IndexError:
                raise StopIteration

            return result

    def __init__(self, keys_count: int):
        self.__keys_count = keys_count
        self.__dictionaries = list()

        for i in range(keys_count):
            self.__dictionaries.append(dict())

    def __new__(cls, keys_count: int):
        if keys_count is None or keys_count < 1:
            return None
        return super().__new__(cls)

    def __iter__(self):
        return self.__Iterator(list(self.__dictionaries[0].values()))

    def __getitem__(self, keys):
        result = None
        node = self.__node_by_keys(list(keys))

        if node is not None:
            result = node.value

        return result

    def __setitem__(self, keys, value):
        self.set(list(keys), value)

    def __delitem__(self, keys):
        self.remove(keys)

    def __node_by_key_at_index(self, key, key_index):
        if key is None or key_index < 0 or key_index >= self.__keys_count:
            raise KeyError('Invalid key: ' + str(key) + ' at index: ' + str(key_index))

        return self.__dictionaries[key_index].get(key)

    def __node_by_keys(self, keys):
        result = None

        if keys is None or len(keys) != self.__keys_count:
            raise KeyError('Invalid keys: ' + str(keys))

        for key_index in range(self.__keys_count):
            key = keys[key_index]
            if key is not None:
                dictionary = self.__dictionaries[key_index]

                if result is not None and dictionary.get(key) != result:
                    result = None
                    break
                else:
                    result = dictionary.get(key)

        return result

    def __remove_node(self, node):
        if node is not None:
            node_keys = node.keys
            dictionaries = self.__dictionaries

            for i in range(self.__keys_count):
                del dictionaries[i][node_keys[i]]

    # PUBLIC

    def get(self, keys: list):
        result = None

        node = self.__node_by_keys(keys)
        if node is not None:
            result = node.value

        return result

    def get_by_key_at_index(self, key, key_index: int):
        result = None

        node = self.__node_by_key_at_index(key, key_index)
        if node is not None:
            result = node.value

        return result

    def remove(self, keys: list):
        node = self.__node_by_keys(keys)
        self.__remove_node(node)

    def remove_by_key_at_index(self, key, key_index: int):
        node = self.__node_by_key_at_index(key, key_index)
        self.__remove_node(node)

    def set(self, keys: list, value):
        node = self.__node_by_keys(keys)

        if node is not None:
            node.value = value
        else:
            node = self.__Node(keys, value)
            if node is None:
                raise KeyError('Invalid keys: ' + str(keys))

            dictionaries = self.__dictionaries

            for i in range(self.__keys_count):
                if dictionaries[i].get(keys[i]) is not None:
                    raise Exception('Value by key: ' + keys[i] + ' at index: ' + str(i) + ' is already exist!!')

            for i in range(self.__keys_count):
                dictionaries[i][keys[i]] = node

    def set_by_key_at_index(self, key, key_index: int, value):
        node = self.__node_by_key_at_index(key, key_index)

        if node is None:
            raise ValueError('Value by key: ' + str(key) + ' at index: ' + str(key_index) + ' is not exist!!')

        node.value = value

    def contains(self, value):
        result = False

        dictionary = self.__dictionaries[0]
        for key in dictionary:
            node = dictionary[key]
            if node.value == value:
                result = True
                break

        return result

    @property
    def count(self):
        return len(self.__dictionaries[0])

    @property
    def keys_count(self):
        return len(self.__dictionaries)
