import os
import random
import select
import socket
import weakref
from abc import ABC, abstractmethod
from configparser import ConfigParser

from Connection import Connection
from Utils.logger import Logger
from Utils.multi_keys_dictionary import MultiKeysDictionary
from Utils.serial_operation_queue import SerialOperationQueue


class ConnectionManagerDelegate(ABC):
    @abstractmethod
    def did_receive_connection(self, connection_identifier: int):
        raise NotImplementedError('This method should be overridden by subclass')

    @abstractmethod
    def did_close_connection(self, connection_identifier: int):
        raise NotImplementedError('This method should be overridden by subclass')

    @abstractmethod
    def did_receive_data(self, received_data: bytes, connection_identifier: int):
        raise NotImplementedError('This method should be overridden by subclass')


class ConnectionManagerConfiguration:
    __LOCAL_HOST_KEY = 'LOCALHOST'
    __PORT_KEY = 'PORT'
    __LISTENING_COUNT_KEY = 'LISTENING_COUNT'
    __MESSAGE_SEPARATOR_KEY = 'MESSAGE_SEPARATOR'
    __RECEIVE_BUFFER_SIZE_KEY = 'RECEIVE_BUFFER_SIZE'

    __CONNECTION_MANAGER_CONFIGURATION_SECTION_NAME = 'CONNECTION_MANAGER_CONFIGURATION'

    def __init__(self, localhost, port, listening_count, message_separator, receive_buffer_size):
        self.__localhost = localhost
        self.__port = port
        self.__listening_count = listening_count
        self.__message_separator = message_separator
        self.__receive_buffer_size = receive_buffer_size

    def __new__(cls, localhost, port, listening_count, message_separator, receive_buffer_size):
        if localhost is not None and len(localhost) > 0 \
                and port is not None and port > 0 \
                and listening_count is not None and listening_count > 0 \
                and message_separator is not None and len(message_separator) > 0 \
                and receive_buffer_size is not None and receive_buffer_size > 0:
            return super().__new__(cls)
        else:
            return None

    def __str__(self):
        return '<localhost=' + str(self.localhost) \
               + '; port=' + str(self.port) \
               + '; listening_count=' + str(self.listening_count) \
               + '>'

    @classmethod
    def configuration_with_file_name(cls, file_name: str):
        configuration = None

        if file_name is not None and os.path.isfile(file_name):
            configuration = None
            config_parser = ConfigParser()
            config_parser.read(file_name)
            connection_manager_configuration = config_parser[cls.__CONNECTION_MANAGER_CONFIGURATION_SECTION_NAME]
            if connection_manager_configuration is not None:
                try:
                    localhost = connection_manager_configuration[cls.__LOCAL_HOST_KEY]
                    port = int(connection_manager_configuration[cls.__PORT_KEY])
                    message_separator = connection_manager_configuration[
                        cls.__MESSAGE_SEPARATOR_KEY].encode().decode('unicode_escape')
                    receive_buffer_size = int(connection_manager_configuration[cls.__RECEIVE_BUFFER_SIZE_KEY])
                    listening_count = int(connection_manager_configuration[cls.__LISTENING_COUNT_KEY])
                    configuration = ConnectionManagerConfiguration(localhost, port, listening_count, message_separator,
                                                                   receive_buffer_size)
                except Exception as e:
                    Logger.log(e)

        return configuration

    @property
    def localhost(self):
        return self.__localhost

    @property
    def port(self):
        return self.__port

    @property
    def listening_count(self):
        return self.__listening_count

    @property
    def message_separator(self):
        return self.__message_separator

    @property
    def receive_buffer_size(self):
        return self.__receive_buffer_size

    @property
    def is_valid(self):
        return self.localhost is not None \
               and self.port is not None \
               and self.listening_count is not None


class ConnectionManager:
    __CONNECTION_MANAGER_THREAD_NAME = 'connection_manager_thread'

    __READONLY_CONNECTION_CONFIGURATION = select.EPOLLIN | select.EPOLLET
    __READ_WRITE_CONNECTION_CONFIGURATION = select.EPOLLIN | select.EPOLLOUT | select.EPOLLET

    def __init__(self):
        self.__delegate = None
        self.__serial_queue = SerialOperationQueue()
        self.__configuration = None
        self.__epoll = None
        self.__server_socket = None
        self.__encoded_message_separator = None

        self.__connections = MultiKeysDictionary(2)

    def __del__(self):
        for connection in self.__connections:
            connection.close()

    @staticmethod
    def __generate_int_identifier():
        return random.getrandbits(32)

    def __handle_connections(self):
        events = self.__epoll.poll(0.01)
        for fileno, event in events:
            if fileno == self.__server_socket.fileno():
                try:
                    while True:
                        sock, _ = self.__server_socket.accept()
                        sock.setblocking(0)
                        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

                        identifier = None
                        while identifier is None:
                            identifier = self.__generate_int_identifier()
                            if self.__connections[identifier, None] is not None:
                                identifier = None

                        new_connection = Connection(sock, identifier, self.__encoded_message_separator)
                        self.__epoll.register(new_connection.fileno(), self.__READ_CONNECTION_CONFIGURATION)
                        self.__connections[new_connection.identifier, new_connection.fileno()] = new_connection

                        if self.delegate is not None:
                            self.__serial_queue.add_executable(self.delegate.did_receive_connection, False,
                                                               **{'connection_identifier': new_connection.identifier})
                except socket.error:
                    pass

            elif event & select.EPOLLIN:
                try:
                    readable_connection = self.__connections[None, fileno]
                    while True:
                        received_data, connection_closed = \
                            readable_connection.receive(self.__configuration.receive_buffer_size)

                        if self.delegate is not None:
                            for received_message in received_data:
                                self.__serial_queue.add_executable(self.delegate.did_receive_data, False,
                                                                   **{
                                                                       'received_data': received_message,
                                                                       'connection_identifier':
                                                                           readable_connection.identifier
                                                                   })
                            if connection_closed:
                                self.__serial_queue.add_executable(self.delegate.did_close_connection, False,
                                                                   **{
                                                                       'connection_identifier':
                                                                           readable_connection.identifier
                                                                   })
                        if connection_closed:
                            self.__epoll.unregister(fileno)
                except socket.error:
                    pass

            elif event & select.EPOLLOUT:
                try:
                    writable_connection = self.__connections[None, fileno]
                    writable_connection.flush()
                except socket.error:
                    pass

            elif event & select.EPOLLHUP:
                hup_connection = self.__connections[None, fileno]
                Logger.log('invalid connection : ' + str(hup_connection))
                hup_connection.close()
                self.delegate.did_close_connection(hup_connection.identifier)

    # PUBLIC

    @property
    def delegate(self):
        result = None
        if self.__delegate is not None:
            result = self.__delegate()
        return result

    @delegate.setter
    def delegate(self, delegate: ConnectionManagerDelegate):
        if delegate is None:
            self.__delegate = None
        else:
            self.__delegate = weakref.ref(delegate)

    @property
    def running(self):
        result = False

        if self.__serial_queue is not None:
            result = self.__serial_queue.running

        return result

    @property
    def configuration(self):
        return self.__configuration

    @configuration.setter
    def configuration(self, configuration: ConnectionManagerConfiguration):
        if not self.running:
            self.__configuration = configuration
        else:
            raise Exception('Setup new configuration while ConnectionManager is running!')

    def start(self):
        configuration = self.__configuration

        if not self.running and configuration is not None and configuration.is_valid:
            self.__encoded_message_separator = configuration.message_separator.encode()

            try:
                self.__server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.__server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.__server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

                address = (configuration.localhost, configuration.port)

                self.__epoll = select.epoll()
                self.__epoll.register(self.__server_socket.fileno(), self.__READONLY_CONNECTION_CONFIGURATION)

                try:
                    self.__server_socket.bind(address)
                    self.__server_socket.listen(configuration.listening_count)
                    self.__server_socket.setblocking(0)
                    try:
                        self.__serial_queue.add_executable(executable=self.__handle_connections, repeat=True)
                        self.__serial_queue.start()
                    finally:
                        self.__server_socket.close()

                except socket.error as e:
                    Logger.log(e)
                    self.stop()
            finally:
                server_socket_fileno = self.__server_socket.fileno()
                if server_socket_fileno is not None and server_socket_fileno >= 0:
                    self.__epoll.unregister(server_socket_fileno)
                self.__epoll.close()
                self.__epoll = None
                self.__server_socket = None

    def stop(self):
        if self.__serial_queue is not None and self.__serial_queue.running:
            self.__serial_queue.stop()

    def send_message(self, message: bytes, connection_identifier: int):
        connection = self.__connections[connection_identifier, None]
        if connection is not None:
            connection.send_message(message)
            self.__epoll.modify(connection.fileno(), self.__READ_WRITE_CONNECTION_CONFIGURATION)

    def close_connection(self, connection_identifier):
        connection = self.__connections[connection_identifier, None]
        if connection is not None:
            connection.close()
